from django.urls import include, path

from rest_framework.routers import DefaultRouter

from .views import circles as circle_views
from .views import memberships as membership_views

router = DefaultRouter()
router.register(r'circles', circle_views.CircleViewSet, basename='circle')

router.register(
    r'circles/(?P<slug_name>[-a-zA-Z0-9_]+)/members',
    membership_views.MembershipViewSet,
    basename='membership'
)

urlpatterns = [
    path('', include(router.urls))
]

# comentado para usar view sets
# from cride.circles import views as circle_views

# urlpatterns = [
#     path('circles/', circle_views.list_circles),
#     path('circles/serializers/model/', circle_views.list_circles_with_serializers_model_instances),
#     path('circles/serializers/queryset/', circle_views.list_circles_with_serializers_query_set),
#     path('circles/create/', circle_views.create_circle),
#     path('circles/create/serializer/', circle_views.create_circle_with_serializer),
# ]