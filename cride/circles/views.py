from rest_framework.decorators import api_view
from rest_framework.response import Response

from cride.circles.models import Circle

from cride.circles.serializers import CircleSerializer, CreateCircleSerializer

@api_view(['GET'])
def list_circles(request):
    circles = Circle.objects.all()
    public = circles.filter(is_public=True)

    data = []
    for circle in public:
        data.append({
            'name': circle.name,
            'slug_name': circle.slug_name,
            'rides_taken': circle.rides_taken,
            'rides_offered': circle.rides_offered,
            'members_limit': circle.members_limit,
        })
    return Response(data)

@api_view(['GET'])
def list_circles_with_serializers_model_instances(request):
    circles = Circle.objects.all()
    public = circles.filter(is_public=True)
    data_model_instances = []
    for circle in public:
        circle_serializer = CircleSerializer(circle)
        data_model_instances.append(circle_serializer.data)

    return Response(circle_serializer.data)

@api_view(['GET'])
def list_circles_with_serializers_query_set(request):
    circles = Circle.objects.all()
    public = circles.filter(is_public=True)
    circle_serializer = CircleSerializer(public, many=True)
    return Response(circle_serializer.data)


@api_view(['POST'])
def create_circle(request):
    name = request.data['name']
    slug_name = request.data['slug_name']
    about = request.data['about']

    circle = Circle.objects.create(
        name=name,
        slug_name=slug_name,
        about=about,
    )
    
    data = []
    data.append({
        'name': circle.name,
        'slug_name': circle.slug_name,
        'rides_taken': circle.rides_taken,
        'rides_offered': circle.rides_offered,
        'members_limit': circle.members_limit,
    })

    return Response(data)

@api_view(['POST'])
def create_circle_with_serializer(request):
    create_circle_serializer = CreateCircleSerializer(data=request.data)
    create_circle_serializer.is_valid(raise_exception=True)
    circle = create_circle_serializer.save()
    # circle = Circle.objects.create(**create_circle_serializer.data)
    return Response(CircleSerializer(circle).data)