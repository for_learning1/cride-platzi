from django.test import TestCase

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from cride.circles.models import Circle, Invitation, Membership
from cride.users.models import User, Profile

# SI ES NECESARIO, MIRAR LA LIBRERIA FACTORY BOY

class InvitationsManagerTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            first_name='Pablo',
            last_name='Trinidad',
            email='pablotrinidad@ciencias.unam.mx',
            username='pablotrinidad',
            password='admin123'
        )
        self.circle = Circle.objects.create(
            name='Facultad de Ciencias',
            slug_name='fciencias',
            about='Grupo oficial de la facultad de ciencias de la UNAM',
            verified=True
        )


    def test_code_generation(self):
        invitation = Invitation.objects.create(
            issued_by=self.user,
            circle=self.circle
        )
        self.assertIsNotNone(invitation.code)


    def test_code_usage(self):

        code = 'holamundo'

        invitation = Invitation.objects.create(
            issued_by=self.user,
            circle=self.circle,
            code=code
        )

        self.assertEqual(invitation.code, code)

    def test_code_generation_if_duplicated(self):

        code = Invitation.objects.create(
            issued_by=self.user,
            circle=self.circle
        ).code

        invitation = Invitation.objects.create(
            issued_by=self.user,
            circle=self.circle,
            code=code
        )

        self.assertNotEqual(invitation.code, code)


class MemberInvitationsAPITestCast(APITestCase):

    def setUp(self):
        self.user = User.objects.create(
            first_name='Pablo',
            last_name='Trinidad',
            email='pablotrinidad@ciencias.unam.mx',
            username='pablotrinidad',
            password='admin123'
        )
        self.profile=Profile.objects.create(
            user=self.user
        )
        self.circle = Circle.objects.create(
            name='Facultad de Ciencias',
            slug_name='fciencias',
            about='Grupo oficial de la facultad de ciencias de la UNAM',
            verified=True
        )
        self.membership = Membership.objects.create(
            user=self.user,
            profile=self.user.profile,
            circle=self.circle,
            remaining_invitations=10
        )
        self.token = Token.objects.create(
            user=self.user
        ).key
        self.client.credentials(HTTP_AUTHORIZATION='Token {}'.format(self.token))
        self.url = '/circles/{}/members/{}/invitations/'.format(
            self.circle.slug_name,
            self.user.username
        )

    def test_response_success(self):
        request = self.client.get(self.url)
        self.assertEqual(request.status_code, status.HTTP_200_OK)

    def test_invitation_creation(self):

        self.assertEqual(Invitation.objects.count(), 0)

        request = self.client.get(self.url)
        self.assertEqual(request.status_code, status.HTTP_200_OK)

        invitations = Invitation.objects.filter(issued_by=self.user)
        self.assertEqual(invitations.count(), self.membership.remaining_invitations)

        for invitation in invitations:
            self.assertIn(invitation.code, request.data['invitations'])




