

from django.db import models

import random
from string import ascii_uppercase, digits

def get_random_code():
    pool = ascii_uppercase + digits + '.-'
    return ''.join(random.choices(pool, k=10))

class InvitationManager(models.Manager):

    def create(self, **kwargs):
        code = kwargs.get('code', get_random_code())

        while self.filter(code=code).exists():
            code = get_random_code()
        kwargs['code'] = code

        return super(InvitationManager, self).create(**kwargs)
