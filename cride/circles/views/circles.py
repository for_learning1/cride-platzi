from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated

from cride.circles.permissions.circles import IsCircleAdmin

from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from cride.circles.serializers import CircleModelSerializer

from cride.circles.models import Circle, Membership

class CircleViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
# ModelViewSet hereda todos los mixin
# se comenta para que quede de muestra
# en el ejercicio se va a ser que el view set considere todas las acciones
# menos borrar
# class CircleViewSet(viewsets.ModelViewSet):

    # se comenta este porque el metodo "get_queryset" lo define
    # queryset = Circle.objects.all()


    serializer_class = CircleModelSerializer
    lookup_field = 'slug_name'
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    search_fields = ('slug_name', 'name')
    ordering_fields = (
        'rides_offered',
        'rides_taken',
        'name',
        'created',
        'members_limit',
        'about'
    )
    ordering = (
        '-members',
        '-rides_offered',
        '-rides_taken'
    )
    filter_fields = (
        'verified',
        'is_limited'
    )

    # se comenta este porque el metodo "get_permissions" lo define
    # permission_classes = (IsAuthenticated,)

    def get_permissions(self):
        permissions = [IsAuthenticated]

        if self.action in ['update', 'partial_update']:
            permissions.append(IsCircleAdmin)

        return [permission() for permission in permissions]
        

    def get_queryset(self):

        queryset = Circle.objects.all()
        if self.action == 'list':
            return queryset.filter(is_public=True)
        return queryset

    def perform_create(self, serializer):

        circle = serializer.save()
        user = self.request.user
        profile = user.profile

        Membership.objects.create(
            user=user,
            profile=profile,
            circle=circle,
            is_admin=True,
            remaining_invitations = 10
        )



