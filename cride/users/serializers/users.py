from django.conf import settings
from django.contrib.auth import authenticate, password_validation
# from django.core.mail import EmailMultiAlternatives
from django.core.validators import RegexValidator
# from django.template.loader import render_to_string
# from django.utils import timezone

from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

# from datetime import timedelta
import jwt

from cride.taskapp.tasks import send_confirmation_email

from cride.users.serializers.profiles import ProfileModelSerializer

from cride.users.models import User, Profile

class UserModelSerializer(serializers.ModelSerializer):

    profile = ProfileModelSerializer(read_only=True)

    class Meta:

        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'profile'
        )

class UserSignUpSerializer(serializers.Serializer):

    email = serializers.EmailField(
        validators=[
            UniqueValidator(queryset=User.objects.all())
        ]
    )

    username = serializers.CharField(
        min_length=4,
        max_length=20,
        validators=[
            UniqueValidator(queryset=User.objects.all())
        ]
    )

    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message='Phone number must be entered in the format: +99999999. Up to 15 digits allowed.'
    )
    phone_number = serializers.CharField(
        validators=[phone_regex],
    )

    password = serializers.CharField(min_length=8, max_length=64)
    password_confirmation = serializers.CharField(min_length=8, max_length=64)

    first_name = serializers.CharField(min_length=2, max_length=30)
    last_name = serializers.CharField(min_length=2, max_length=30)

    def validate(self, data):
        password = data['password']
        password_confirmation = data['password_confirmation']
        if password != password_confirmation:
            raise serializers.ValidationError('Passwords does not match')
        password_validation.validate_password(password)
        return data

    def create(self, data):
        data.pop('password_confirmation')
        user = User.objects.create_user(**data, is_verified=False, is_client=True)
        profile = Profile.objects.create(user=user)
        send_confirmation_email.delay(user_pk=user.pk)
        # self.send_confirmation_email(user)
        return user

    # se comenta esta funcionalidad para aplicarla con Celery

    # def send_confirmation_email(self, user):
    #     token = self.get_verification_token(user)

    #     subject = 'Welcome @{}! Verify your account to start using Comparte Ride'.format(user.username)
    #     from_email = 'Comparte Ride <noreply@comparteride.com>'
    #     content = render_to_string(
    #         'emails/users/account_verification.html',
    #         {
    #             'token': token,
    #             'user': user
    #         }
    #     )
    #     msg = EmailMultiAlternatives(subject, content, from_email, [user.email])
    #     msg.attach_alternative(content, 'text/html')
    #     msg.send()


    # def get_verification_token(self, user):
    #     exp_date = timezone.now() + timedelta(days=3)
    #     payload = {
    #         'user': user.username,
    #         'exp': int(exp_date.timestamp()),
    #         'type': 'email_confirmation'
    #     }
    #     token = jwt.encode(
    #         payload,
    #         settings.SECRET_KEY,
    #         algorithm='HS256'
    #     )
    #     return token.decode()



class UserLoginSerializer(serializers.Serializer):

    email = serializers.EmailField()
    password = serializers.CharField(min_length=8, max_length=64)

    def validate(self, data):
        user = authenticate(username=data['email'], password=data['password'])
        if not user:
            raise serializers.ValidationError('Invalid credentials')
        if not user.is_verified:
            raise serializers.ValidationError('Account is not active yet')

        self.context['user'] = user
        return data

    def create(self, data):
        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key

class AccountVerificationSerializer(serializers.Serializer):

    token = serializers.CharField()

    def validate_token(self, data):

        try:
            payload = jwt.decode(data, settings.SECRET_KEY, algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise serializers.ValidationError('Verification link has expired')
        except jwt.PyJWTError:
            raise serializers.ValidationError('Invalid token')
        if payload['type'] != 'email_confirmation':
            raise serializers.ValidationError('Invalid token')

        self.context['payload'] = payload
        return data

    def save(self):

        payload = self.context['payload']
        user = User.objects.get(username=payload['user'])
        user.is_verified = True
        user.save()

