from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext as _

from cride.users.models import User, Profile

class CustomUserAdmin(UserAdmin):

    list_display = (
        'email',
        'username',
        'first_name',
        'last_name',
        'is_staff',
        'is_client',
        'is_verified'
    )

    list_filter = (
        'is_staff',
        'is_client',
        'is_verified',
        'created',
        'modified'
    )

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'is_verified', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):

    list_display = (
        'user',
        'reputation',
        'rides_taken',
        'rides_offered'
    )

    search_fields = (
        'user__username',
        'user__email',
        'user__first_name',
        'user__last_name'
    )

    list_filter = (
        'reputation',
    )

admin.site.register(User, CustomUserAdmin)