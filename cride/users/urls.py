from django.urls import include, path

from rest_framework.routers import DefaultRouter

from .views import users as user_views

router = DefaultRouter()
router.register(r'users', user_views.UserViewSet, basename='users')

urlpatterns = [
    path('', include(router.urls))
]

# se comentan las siguientes lineas porque se implemento un viewset para users

# from cride.users.views import AccountVerification, UserLoginAPIView, UserSignUpAPIView

# urlpatterns = [
#     path('users/login/', UserLoginAPIView.as_view(), name='login'),
#     path('users/signup/', UserSignUpAPIView.as_view(), name='signup'),
#     path('users/verify/', AccountVerification.as_view(), name='verify'),
# ]