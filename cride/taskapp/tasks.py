from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone

from celery.decorators import task, periodic_task

from datetime import timedelta
import jwt
import time

from cride.users.models import User

def get_verification_token(user):
    exp_date = timezone.now() + timedelta(days=3)
    payload = {
        'user': user.username,
        'exp': int(exp_date.timestamp()),
        'type': 'email_confirmation'
    }
    token = jwt.encode(
        payload,
        settings.SECRET_KEY,
        algorithm='HS256'
    )
    return token.decode()

@task(name='send_confirmation_email', max_retries=3)
def send_confirmation_email(user_pk):

    for i in range(30):
        time.sleep(1)
        print('sleeping', str(i+1))

    user = User.objects.get(pk=user_pk)

    token = get_verification_token(user)

    subject = 'Welcome @{}! Verify your account to start using Comparte Ride'.format(user.username)
    from_email = 'Comparte Ride <noreply@comparteride.com>'
    content = render_to_string(
        'emails/users/account_verification.html',
        {
            'token': token,
            'user': user
        }
    )
    msg = EmailMultiAlternatives(subject, content, from_email, [user.email])
    msg.attach_alternative(content, 'text/html')
    msg.send()

# @periodic_task(name='disabled_finished_rides', run_every=timedelta(seconds=5))
# def disabled_finished_rides():
#     print('hello marlon')