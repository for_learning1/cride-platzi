from django.contrib import admin

from cride.rides.models import Ride

@admin.register(Ride)
class RideAdmin(admin.ModelAdmin):

    list_display = (
        'offered_by',
        'offered_in',
        'available_sets',
        'departure_location',
        'departure_date',
        'arrival_location',
        'arrival_date',
    )


    # search_fields = (
    #     'slug_name',
    #     'name'
    # )

    # list_filter = (
    #     'is_public',
    #     'verified',
    #     'is_limited',
    # )