from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticated
from cride.circles.permissions.memberships import isActiveCircleMember
from cride.rides.permissions.rides import IsRideOwner, IsNotRideOwner

from rest_framework.filters import SearchFilter, OrderingFilter

from cride.rides.serializers import (
    CreateRideModelSerializer,
    JoinRideModelSerializer,
    RideModelSerializer,
    # EndRideSerializer
)

from cride.circles.models import Circle

from datetime import timedelta
from django.utils import timezone

class RideViewSet (
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        mixins.UpdateModelMixin,
        viewsets.GenericViewSet
    ):

    # serializer_class = CreateRideModelSerializer
    # permission_classes = [IsAuthenticated, isActiveCircleMember]

    filter_backends = (SearchFilter, OrderingFilter)

    ordering = ('departure_date', 'arrival_date', 'available_sets')
    ordering_fields = ('departure_date', 'arrival_date', 'available_sets')
    search_fields = ('departure_location', 'arrival_location')


    def dispatch(self, request, *args, **kwargs):
        slug_name = kwargs['slug_name']
        self.circle = get_object_or_404(Circle, slug_name=slug_name)
        return super(RideViewSet, self).dispatch(request, *args, **kwargs)

    def get_permissions(self):
        permissions = [IsAuthenticated, isActiveCircleMember]
        if self.action in ['update', 'partial_update', 'finish']:
            permissions.append(IsRideOwner)
        if self.action == 'join':
            permissions.append(IsNotRideOwner)

        return [permission() for permission in permissions]

    def get_serializer_context(self):
        context = super(RideViewSet, self).get_serializer_context()
        context['circle'] = self.circle
        return context

    def get_serializer_class(self):

        if self.action == 'create':
            return CreateRideModelSerializer

        if self.action == 'update':
            return JoinRideModelSerializer

        # if self.action == 'finish':
        #     return EndRideSerializer
        
        return RideModelSerializer

    def get_queryset(self):
        offset = timezone.now() + timedelta(minutes=30)
        return self.circle.ride_set.filter(
            departure_date__gte=offset,
            available_sets__gt=1
        )

    @action(detail=True, methods=['post'])
    def join(self, request, *args, **kwargs):

        ride = self.get_object()

        serializer_class = self.get_serializer_class()
        serializer = serializer_class(
        # serializer = JoinRideModelSerializer(
            ride,
            data={'passenger': request.user.pk},
            context={'ride': ride, 'circle': self.circle},
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        ride = serializer.save()
        data = RideModelSerializer(ride).data
        return Response(data, status=status.HTTP_200_OK)
